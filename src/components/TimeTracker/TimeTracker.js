import React, { useState, useEffect, useRef } from 'react'
import { v4 as uuidv4 } from 'uuid';

import './TimeTracker.css'
import AddTask from '../Tasks/AddTask/AddTask'
import ShowTasks from '../Tasks/ShowTasks/ShowTasks';
import Modal from '../UI/Modal/Modal'

const TimeTracker = () => {

    const [openModal, setOpenModal] = useState(false)
    const [tasks, setTasks] = useState([])
    const [editTask, setEditTask] = useState({})
    const [searchTerm, setSearchTerm] = useState('')
    const [serachResults, setSearchResults] = useState([])

    const openTaskHandler = () => {
        setOpenModal(true)
        setEditTask({})
    }

    const cancelTaskHandler = () => {
        setOpenModal(false)
    }

    useEffect(() => {
        getTasks()
    }, [])

    useEffect(() => {
        const results = tasks.filter(task => task.title.toLowerCase().includes(searchTerm));
        setSearchResults(results)
    }, [searchTerm])

    const getTasks = () => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')
            dataStore.getAll().onsuccess = function (event) {
                const result = event.target.result
                if (result && result.length > 0) {
                    setTasks(result)
                    setSearchResults(result)
                } else {
                    setTasks([])
                }
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('tasks', { keyPath: 'id' })
        }
    }

    const addTaskHandler = (title, description, tag) => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')

            let id = uuidv4()
            let showTimer = false;

            dataStore.put({ id, title, description, tag, showTimer }).onsuccess = function (event) {
                window.indexedDB.open('Tasks', 1);
                cancelTaskHandler()
                getTasks()
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('tasks', { keyPath: 'id' })
        }
    }

    const updateTaskHandler = (title, description, tag, task) => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')

            let showTimer = task.showTimer;
            let id = task.id
            dataStore.put({ id, title, description, tag, showTimer }).onsuccess = function (event) {
                window.indexedDB.open('Tasks', 1);
                cancelTaskHandler()
                getTasks()
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('tasks', { keyPath: 'id' })
        }
    }

    const removeTaskHandler = (id) => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')
            dataStore.delete(id).onsuccess = function (event) {
                window.indexedDB.open('Tasks', 1);
                getTasks()
            }
            request.onupgradeneeded = function (event) {
                const db = event.target.result
                db.createObjectStore('tasks', { keyPath: 'id' })
            }
        }
    }

    const startTimer = ({ id, title, description, tag, showTimer }) => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')

            showTimer = true
            dataStore.put({ id, title, description, tag, showTimer }).onsuccess = function (event) {
                window.indexedDB.open('Tasks', 1);
                getTasks()
            }
            request.onupgradeneeded = function (event) {
                const db = event.target.result
                db.createObjectStore('tasks', { keyPath: 'id' })
            }
        }
    }

    const stopTimer = ({ id, title, description, tag, showTimer }) => {
        const request = window.indexedDB.open('Tasks', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('tasks', 'readwrite')
            const dataStore = transaction.objectStore('tasks')

            showTimer = false
            dataStore.put({ id, title, description, tag, showTimer }).onsuccess = function (event) {
                window.indexedDB.open('Tasks', 1);
                getTasks()
            }
            request.onupgradeneeded = function (event) {
                const db = event.target.result
                db.createObjectStore('tasks', { keyPath: 'id' })
            }
        }
    }

    const editTaskHandler = (task) => {
        setOpenModal(true)
        setEditTask(task)
    }

    const searchBarHandler = event => {
        setSearchTerm(event.target.value)
    }



    let showtasks = <p className="not_task_title">No Tasks !!!</p>

    if (serachResults.length > 0) {
        showtasks = <ShowTasks tasks={serachResults} removeTaskHandler={removeTaskHandler} startTimer={startTimer} stopTimer={stopTimer} editTaskHandler={editTaskHandler} />
    }

    return (
        <div className="time_tracking_header">
            <div className="time_tracking_add" onClick={openTaskHandler}>
                <div className="fa fa-plus" style={{ color: '#2b5f8d', marginRight: '15px' }}></div>
                Add Task
            </div>
            {openModal && <Modal show={openModal} modalClosed={cancelTaskHandler}>
                <AddTask closeTaskHandler={cancelTaskHandler} createTaskHandler={addTaskHandler} editTask={editTask} updateTaskHandler={updateTaskHandler} />
            </Modal>}
            <div className="search_bar">
                <input key="search" placeholder="Enter Title to search" value={searchTerm} onChange={event => searchBarHandler(event)} className="search_input"/>
                <div className="fa fa-search" style={{ color: '#2b5f8d', marginLeft: '15px' }}></div>
            </div>
            {showtasks}
        </div>
    )
}

export default TimeTracker