import React, { useState } from 'react'

import './AddTask.css';

import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';

const AddTask = props => {

    const tags = [
        { id: 1, displayValue: 'Select Tag', value: "" },
        { id: 2, displayValue: 'Pending', value: 'Pending' },
        { id: 3, displayValue: 'Study', value: 'Study' },
        { id: 4, displayValue: 'Personal', value: 'Personal' },
        { id: 5, displayValue: 'Work', value: 'Work' },
        { id: 6, displayValue: 'PartTime', value: 'PartTime' },
        { id: 7, displayValue: 'Completed', value: 'Completed' },
    ]

    const [title, setTitle] = useState(props.editTask.title || '')
    const [description, setDescription] = useState(props.editTask.description || '')
    const [tag, setTag] = useState(props.editTask.tag || '')

    const inputHandler = (event, id) => {
        if (id === 'title') {
            setTitle(event.target.value)
        }
        if (id === 'desc') {
            setDescription(event.target.value)
        }
        if (id === 'tag') {
            setTag(event.target.value)
        }
    }

    return (
        <div>
            <div className="add_task_main">
                <Input label="Enter Title" key="title" type="text" placeholder="Enter Title" key="title" value={title} onChange={event => inputHandler(event, 'title')} />
                <Input label="Enter Description" type="text" placeholder="Enter Description" key="description" value={description} onChange={event => inputHandler(event, 'desc')} />
                <div className="task_tag">
                    <label className="task_label">Add Tag</label>
                    <select className="task_tag_select" value={tag} onChange={event => inputHandler(event, 'tag')}>
                        {tags.map(tag => (
                            <option key={tag.id} placeholder="Select Tag" value={tag.value}>
                                {tag.displayValue}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="add_task_actions">
                    {
                        Object.keys(props.editTask).length > 0 ? <Button btnType="save_button" clicked={() => props.updateTaskHandler(title, description, tag, props.editTask)}>Update</Button> :
                            <Button btnType="save_button" clicked={() => props.createTaskHandler(title, description, tag)}>Create</Button>
                    }

                    <Button btnType="reset_button" clicked={props.closeTaskHandler}>Cancel</Button>
                </div>
            </div>
        </div>
    )
}

export default AddTask