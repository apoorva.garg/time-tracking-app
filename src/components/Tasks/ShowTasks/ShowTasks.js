import React from 'react'

import './ShowTask.css';
import Button from '../../UI/Button/Button';
import Timer from '../../Timer/Timer';

const ShowTasks = props => {
    return (
        <div>
            {
                props.tasks.map(task => {
                    return (
                        <div className="show_tasks_header" key={task.id}>
                            <div className="show_tasks_header_main">
                                <p className="show_tasks_title">{task.title}</p>
                                <p className="show_tasks_tag">{task.tag}</p>
                            </div>
                            <p className="show_tasks_desciption">{task.description}</p>
                            <Timer task={task} />
                            <div className="show_tasks_actions">
                                <Button btnType="save_button" clicked={() => props.editTaskHandler(task)}>Edit</Button>
                                <Button btnType="save_button" clicked={() => props.removeTaskHandler(task.id)}>Remove</Button>
                            </div>
                            <div className="show_task_timer">
                                {task.showTimer ? <Button btnType="full_button" clicked={() => props.stopTimer(task)}>STOP</Button> : <Button btnType="full_button" clicked={() => props.startTimer(task)}>START</Button>}
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default ShowTasks