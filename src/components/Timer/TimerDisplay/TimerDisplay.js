import React from 'react'

import './TimerDisplay.css';

const TimerDisplay = props => {
    return (
        <div className="timer_display">
            {props.formatTime(props.minutes)}:
            {props.formatTime(props.seconds)}:
            {props.formatTime(props.miliSeconds, 'ms')}
        </div>
    );
}

export default TimerDisplay;