import React, { useState, useEffect } from 'react'

import './Timer.css';

import TimerDisplay from './TimerDisplay/TimerDisplay'

const Timer = props => {
    const [timeMiliSecond, setMiliSeconds] = useState(0);
    const [timeSecond, setTimeSeconds] = useState(0);
    const [timeMinutes, setTimeMinutes] = useState(0);

    const formatTime = (val, ...rest) => {
        let value = val.toString();
        if (value.length < 2) {
            value = '0' + value;
        }
        if (rest[0] === 'ms' && value.length < 3) {
            value = '0' + value;
        }
        return value;
    }

    useEffect(() => {
        let interval;
        if (props.task.showTimer) {
            interval = setInterval(
                () => pace(), 10
            );
        }
        return () => clearInterval(interval);
    })

    const pace = () => {
        setMiliSeconds(timeMiliSecond + 10)
        if (timeMiliSecond >= 1000) {
            setTimeSeconds(timeSecond + 1)
            setMiliSeconds(0)
        }
        if (timeSecond >= 60) {
            setTimeMinutes(timeMinutes + 1)
            setTimeSeconds(0)
        }
    };

    return (
        <div className="timer_header">
            <TimerDisplay miliSeconds={timeMiliSecond} minutes={timeMinutes} seconds={timeSecond} formatTime={formatTime} />
        </div>
    )
}

export default Timer