import React from 'react'

import './Input.css';

const input = (props) => (
    <div className="Input">
        <label className="Label">{props.label}</label>
        <input className="InputElement" {...props} value={props.value} />
    </div>
);

export default input