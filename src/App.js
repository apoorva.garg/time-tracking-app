import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/Navigation/MainNavigation';
import TimeTracker from './components/TimeTracker/TimeTracker';

const app = () => {
  return (
    <React.Fragment>
      <MainNavigation />
      <main className="main-content">
        <Switch>
          <Redirect from="/" to="/timetracker" exact />
          <Route path="/timetracker" component={TimeTracker} />
        </Switch>
      </main>
    </React.Fragment>
  );
}

export default app;
